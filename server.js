const config = require('./server/config');

var express = require('express')
var bodyParser = require('body-parser');
var app = express()
var port = process.env.PORT || 3000
var server = require('http').createServer(app)
var jwt = require('./lib/JWT')
var _ = require('underscore')

app.use(bodyParser.json({
    limit: '5mb'
}));
app.use(bodyParser.urlencoded({
    limit: '5mb',
    extended: true
}));

app.use(function (req, res, next) {
    if (_.indexOf(config.NoJWTUrl, req.originalUrl) == -1) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        if (token) {
            console.log('Decoding token . . . ')
            var decoded = jwt.decode(token)
            if (decoded.status != 1) {
                console.log(decoded)
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                })
            } else {
                req.decoded = decoded
                next()
            }
        } else {
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            })
        }
    } else {
        next()
    }
})

var authRoute = require('./routes/auth')(app, config);
var sampleRoute = require('./routes/sample')(app, config);

server.listen(port, function () {
    console.log("Listening on port %s...", server.address().port)
});