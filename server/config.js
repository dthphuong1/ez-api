var CONFIG_TYPE = 0;

switch (CONFIG_TYPE) {
    case 0: // local
        var server = "localhost",
            port = "27017",
            username = "admin",
            password = "<password>",
            dbName = "my-database";

        exports.connectionString = 'mongodb://' + username + ':' + password + '@' + server + ':' + port + '/' + dbName;
        break;
    case 1: // demo server
        var server = "localhost",
            port = "27017",
            username = "admin",
            password = "<password>",
            dbName = "my-database";

        exports.connectionString = 'mongodb://' + username + ':' + password + '@' + server + ':' + port + '/' + dbName;
        break;
    case 2: // real server
        var server = "localhost",
            port = "27017",
            username = "admin",
            password = "<password>",
            dbName = "my-database";

        exports.connectionString = 'mongodb://' + username + ':' + password + '@' + server + ':' + port + '/' + dbName;
        break;
}


//=====JWT config=====
var secret = "#Secret#";
var exptime = "60";
// =====#END JWT config=====

//=====Mail server config=====
var emailAddress = "noreply@your-email.vn";
var emailPassword = "P@ssw0rd-here";
var smtpPort = 587;
var smtpHost = 'smtp.yandex.com';
// =====#END Mail server config=====

exports.NoJWTUrl = ['/', '/auth'];
exports.secret = secret;
exports.exptime = exptime;
exports.emailAddress = emailAddress;
exports.emailPassword = emailPassword;
exports.smtpHost = smtpHost;
exports.smtpPort = smtpPort;